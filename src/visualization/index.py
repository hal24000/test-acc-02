"""This page is the overview page. It contains the navigation bar and navigation
functionalities. It should import and navigate to the other pages.
"""
# this script will be run twice, once when the index page is imported into runserver.py and once
# when the app.run is called. the second time around the working dir will be the root of the project for some reason
# need to set the working directory to the 'WRKDIR' environment variable set in the runserver.py (project/src)
# so that all scripts assume they are under src/

import os
import dash_core_components as dcc
from dash.dependencies import Input, Output, State
import dash_html_components as html
from .pages import (test_page)
from .app_def import app
import logging
from data.project_loggers import dash_logger

logger = logging.getLogger(dash_logger)

app.layout = html.Div([
    dcc.Location(id='url', refresh=False, pathname='yorkshire-water'),
    html.Div(id='page-content', children=[test_page.content], className="page"), #the main content
])

pages = {
    'test-page':test_page.content,
}
@app.callback(
    Output('page-content', 'children'),
    [Input("url", "pathname")],
)
def change_page( curr_url):
    logger.info("Entered change page code")
    if curr_url == "/" or curr_url == None:
        logger.info("setting to map page")
        return test_page.content

    else:
        content = pages.get(curr_url, test_page.content)
        return content
